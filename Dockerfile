# Node image
FROM 192.207.60.139:5000/java-test:latest

# set working directory
WORKDIR /usr/src/app

# copy source code
COPY . /usr/src/app/


RUN javac src/HelloWorld.java

# run sonarqube
RUN ./sonar-scanner-3.2.0.1227-linux/bin/sonar-scanner 2>&1 | tee sonar.txt && if grep -qF "FAILURE" sonar.txt;then exit 1; fi

RUN rm -rf sonar*

EXPOSE 8080

#RUN javac src/GoodbyeWorld.java
CMD ["java", "-cp", "src", "HelloWorld"]
